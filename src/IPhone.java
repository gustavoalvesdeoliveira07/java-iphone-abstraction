import java.util.HashMap;
import java.util.Map;

public class IPhone {
    private boolean isOn = false;
    private static Map<String, String> conect = new HashMap<String, String>(){{
        put("source", "");
        put("target", "");
    }};
    public ReprodutorMusical player;
    public NavNet internet;

    private String contact = "";

    IPhone(String contact) {
        this.contact = contact;
    }

    public IPhone startPlayer() { this.player = new ReprodutorMusical(); return this; }
    public IPhone startInternet() { this.internet = new NavNet(); return this; }

    public void turnOn() {
        if(!isOn)   this.isOn = true;
        else        this.isOn = false;
    }

    public void call(String phone) {
        if(!this.isOn) {
            System.out.println("Ligue o telefone.");
            return;
        }

        conect.replace("target", phone);
        conect.replace("source", this.contact);
        System.out.println("Chamando " + phone + " ...");
    }

    public void getCall() {
        if(!this.isOn) return;

        if(!conect.get("target").equals("")) {
            System.out.println("Atendendo " + conect.get("source") + " ...");
        }
    }

    public void endCall() {
        if(!conect.get("source").equals("")) System.out.println("Finalizando chamada...");

        conect.replace("source", "");
        conect.replace("target", "");
    }

    public void iniciarCorreioVoz(String email) {
        if(!this.isOn) System.out.println("Iniciando correio para " + email);
    }

    public String getContact() { return this.contact; }
    public void setContact(String contact) { this.contact = contact; }
}
