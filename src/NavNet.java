import java.util.ArrayList;
import java.util.Optional;

public class NavNet {
    private ArrayList<Aba> abas;
    private Aba selectedPage;

    public NavNet() {
        abas = new ArrayList<>(){{
            add(new Aba("Google", "https://www.google.com"));
        }};
        selectedPage = abas.get(0);     // default page
    }

    public void addNewPage(String page, String link) {
        this.abas.add(new Aba(page, link));
    }

    public void showPage() {
        System.out.println("Página: " + this.selectedPage.page);
        System.out.println("Link: " + this.selectedPage.link);
    }

    public void selectPage(String link) {
        Optional<Aba> hasPage = abas.stream().filter(aba -> aba.link.equals(link)).findFirst();

        if(hasPage.isPresent())
            this.selectedPage = hasPage.get();
    }
}
