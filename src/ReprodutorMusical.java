import java.util.LinkedHashMap;
import java.util.Map;

public class ReprodutorMusical {
    private Map<String, Long> musics_durations_seconds = new LinkedHashMap<>(0);
    private boolean isPaused = true;
    private String selectedMusic = "";
    public ReprodutorMusical() {
        
    }

    public void addMusic(String music, Long duration_seconds) {
        musics_durations_seconds.put(music, duration_seconds);
    }

    public void selectMusic(String music) {
        if(musics_durations_seconds.containsKey(music)) this.selectedMusic = music;
        else this.selectedMusic = "";
    }

    private void play() {
        System.out.println("Tocando " + this.selectedMusic);
        System.out.println("Duração " + this.musics_durations_seconds.get(this.selectedMusic) + " segundos "+this.isPaused);
        while(!this.isPaused) { 
            System.out.println("playing...");  
            try {
                Thread.sleep(this.musics_durations_seconds.get(this.selectedMusic)*1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        System.out.println("\nPlayer pausado...");
    }

    public void playMusic() {
        if(this.isPaused) this.isPaused = false;

        Thread playingMusic = new Thread(this::play);
        playingMusic.start();
    }

    public void pause() {
        if(!this.isPaused) this.isPaused = true;
    }
}
