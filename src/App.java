
public class App {
    public static void main(String[] args) throws Exception {
        IPhone iphoneA = new IPhone("+5534996362512");
        IPhone iphoneB = new IPhone("+5534996366334");

        iphoneA.turnOn();
        iphoneB.turnOn();

        iphoneA.call(iphoneB.getContact());
        iphoneB.getCall();

        iphoneA.endCall();

        // -------------------
        iphoneA.startPlayer();
        iphoneA.player.addMusic("Musica A", 4L);
        iphoneA.player.addMusic("Musica B", 2L);
        iphoneA.player.addMusic("Musica C", 5L);

        iphoneA.player.selectMusic("Musica B");
        System.out.println("Tocando Música...\n");
        iphoneA.player.playMusic();
        Thread.sleep(20);
        iphoneA.player.pause();

        // -----------------
        System.out.println("\n\tABRINDO NAVEGADOR");
        iphoneB.startInternet().internet.showPage();
        iphoneB.internet.addNewPage("YoutTube", "https://www.youtube.com");
        iphoneB.internet.selectPage("https://www.youtube.com");
        iphoneB.internet.showPage();
    }
}
